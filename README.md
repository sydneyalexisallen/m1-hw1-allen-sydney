I had no issues.
I used outside resources to help refresh my memory on setting up my HTML and CSS pages. I have listed the links below:
https://www.w3schools.com/html/tryit.asp?filename=tryhtml_default
https://www.geeksforgeeks.org/html-5-header-tag/
https://www.w3schools.com/tags/tag_main.asp
https://www.w3schools.com/tags/tag_a.asp
https://www.w3schools.com/css/css_howto.asp
https://www.w3schools.com/html/html_head.asp
https://www.w3schools.com/html/html_css.asp
https://www.w3schools.com/tags/tag_meta.asp